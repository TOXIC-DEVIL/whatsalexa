RED='\033[0;31m'
GREEN='\033[1;32m'
CYAN='\033[1;36m'
PURPLE='\033[0;35m'
NC='\033[0m'
clear
printf "\n\n${GREEN} Installing WhatsAlexa...\n\n"
apt update && apt upgrade
clear
printf "\n\n${GREEN} Updating WhatsAlexa...\n\n"
pkg upgrade -q -y
clear
printf "\n\n${RED} Installing Baileys...\n\n"
apt install nodejs --fix-missing -qq -y
clear
printf "\n\n${RED} Updating Baileys\n\n"
pkg install git -qq -y
clear
printf "\n\n${CYAN} Installing Chalk...\n\n"
git clone https://github.com/TOXIC-DEVIL/WhatsAlexa -q
clear
cd WhatsAlexa
printf "\n\n${CYAN} Updating Chalk...\n\n"
npm install @adiwajshing/baileys --silent
clear
printf "\n\n${RED} Installing All the Packages...\n\n"
npm install chalk --silent
clear
printf "\n\n${RED} Updating All the Packages...\n\n"
clear
printf "\n\n${GREEN} Ready To Connect!...\n\n"
printf "\n\n${GREEN} ."
printf "\n\n${GREEN} .."
printf "\n\n${GREEN} ..."
clear
node alexa.js
